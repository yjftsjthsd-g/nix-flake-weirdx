# nix-flake-weirdx

A nix flake to build WeirdX, the pure Java X11 server.


## Use

Prerequisite: You'll need a flake-enabled nix system; see https://nixos.wiki/wiki/Flakes for details.

To run:
```
nix run gitlab:yjftsjthsd-g/nix-flake-weirdx
```
Then in another terminal:
```
export DISPLAY=127.0.0.1:2
xterm
```

Or to just get the jar file:
```
nix build gitlab:yjftsjthsd-g/nix-flake-weirdx
```
and you'll have the jar at `./result/share/java/weirdx.jar`. This should be an independent build artifact - you need nix with flakes to build the jar file, but once you have it you can copy it to any system with a compatible JVM and run it without extra dependencies.


## License

This repo - a bunch of build scripts, basically - is released under the MIT
license, but WeirdX itself is under the GPLv2 license, so the resulting jar is
under GPLv2.

