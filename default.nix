{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
	name = "weirdx";
	version = "1.0.32";
	src = fetchurl {
		url = "https://master.dl.sourceforge.net/project/weirdx/WeirdX/${version}/weirdx-${version}.tar.gz";
		sha256 = {
			"1.0.32" = "1d3573a37nm4vm7c7pd1xiwbwf3j463vf9bzpq7s1yhzjr9bj7nx";
		}.${version} or "0000000000000000000000000000000000000000000000000000";
	};
	nativeBuildInputs = [ jdk makeWrapper ];
    buildPhase = ''
      javac -O com/jcraft/weirdx/*.java com/jcraft/util/*.java
    '';
    installPhase = ''
      mkdir -p $out/share/java
      cp misc/weirdx.jar $out/share/java/
      mkdir -p $out/bin
      makeWrapper ${jre}/bin/java $out/bin/weirdx --add-flags "-jar $out/share/java/weirdx.jar"
    '';

	meta = {
		description = "weirdx";
		homepage = "http://www.jcraft.com/weirdx/";
		license = "GPLv2";
	};
}
